/* Resource - Author: Justin Kim, site: youtube.com, date posted: April 4, 2020, title: "Let's Build Connect Four using JavaScript! - Tutorial" */

const allCells = document.querySelectorAll('.cell:not(.row-top)');
const topCells = document.querySelectorAll('.cell.row-top');
const resetButton = document.querySelector('.reset');
const statusSpan = document.querySelector('.status');

// columns
const column0 = [allCells[35], allCells[28], allCells[21], allCells[14], allCells[7], allCells[0], topCells[0]];
const column1 = [allCells[36], allCells[29], allCells[22], allCells[15], allCells[8], allCells[1], topCells[1]];
const column2 = [allCells[37], allCells[30], allCells[23], allCells[16], allCells[9], allCells[2], topCells[2]];
const column3 = [allCells[38], allCells[31], allCells[24], allCells[17], allCells[10], allCells[3], topCells[3]];
const column4 = [allCells[39], allCells[32], allCells[25], allCells[18], allCells[11], allCells[4], topCells[4]];
const column5 = [allCells[40], allCells[33], allCells[26], allCells[19], allCells[12], allCells[5], topCells[5]];
const column6 = [allCells[41], allCells[34], allCells[27], allCells[20], allCells[13], allCells[6], topCells[6]];
const columns = [column0, column1, column2, column3, column4, column5, column6];


// rows
const topRow = [topCells[0], topCells[1], topCells[2], topCells[3], topCells[4], topCells[5], topCells[6]];
const row0 = [allCells[0], allCells[1], allCells[2], allCells[3], allCells[4], allCells[5], allCells[6]];
const row1 = [allCells[7], allCells[8], allCells[9], allCells[10], allCells[11], allCells[12], allCells[13]];
const row2 = [allCells[14], allCells[15], allCells[16], allCells[17], allCells[18], allCells[19], allCells[20]];
const row3 = [allCells[21], allCells[22], allCells[23], allCells[24], allCells[25], allCells[26], allCells[27]];
const row4 = [allCells[28], allCells[29], allCells[30], allCells[31], allCells[32], allCells[33], allCells[34]];
const row5 = [allCells[35], allCells[36], allCells[37], allCells[38], allCells[39], allCells[40], allCells[41]];
const rows = [row0, row1, row2, row3, row4, row5, topRow];

// Game start variables 
let gameIsOn = true;
let blackTurn = true;


// Functions (Util)
// function retrieves class list of cells in an array using spreader operator
const getClassListArray = function (cell) {
    const classList = cell.classList;
    return [...classList]
}

// function retrieves and tracks cell location
const getCellLocation = function (cell) {
    const classList = getClassListArray(cell);
    // using the find method to search 'classList' for 'row' and 'col'
    const rowClass = classList.find(className => className.includes('row'));
    const colClass = classList.find(className => className.includes('col'));
// extract the numbers from the rowClass & colClass
    const rowIndex = rowClass[4];
    const colIndex = colClass[4];
// convert the strings from row and col index into numbers
    const rowNumber = parseInt(rowIndex, 10);
    const colNumber = parseInt(colIndex, 10);
    
    return [rowNumber, colNumber];
}
// function checks first open cell in column
const getFirstOpenCellInColumn = function (colIndex) {
    const column = columns[colIndex];
    // remove top column from columns array
    const columnWithoutTop = column.slice(0, 6);

    // check to see if cell has red class or black class using 'for-of' loop
    // if it does then it should check next open cell
    for (const cell of columnWithoutTop) {
        const classList = getClassListArray(cell);
        if (!classList.includes('black') && !classList.includes('red')) {
            return cell;
        }
    }
    return null; // to show the column has no openings
}

// function acts as toggle of the hovering player's disc color so it switches after the click and drop of a disc
const clearColorFromTop = function(colIndex) {
    const topCell = topCells[colIndex];
    topCell.classList.remove('black');
    topCell.classList.remove('red');
}

// function gets the color of cell disc in order to track status of the game
const getColorOfCell = function(cell) {
    const classList = getClassListArray(cell);
    if (classList.includes('black')) {
        return 'black';
    } else if (classList.includes('red')) {
        return 'red';
    } else {
        return null;
    }
}

// function checks the winning conditions of pieces dropped and returns a boolean
const checkWinningCells = function (cells) {
    if (cells.length < 4) {
        return false;
    }
    gameIsOn = false;
    for (const cell of cells) {
        cell.classList.add('win')
    }
    // update the status span so the winner is displayed to the page using es6 concat in tenary format
    statusSpan.textContent = `${blackTurn ? 'Black' : 'Red'} has won!`;

    return true;
}

// function checks the status of the game
const checkStatusOfGame = function(cell) {
    const color = getColorOfCell(cell);
    if (!color) {
        return;
    }
    const [rowIndex, colIndex] = getCellLocation(cell);

    // Check horizontally
    let winningCells = [cell];
    let rowToCheck = rowIndex;
    let colToCheck = colIndex - 1; // start from the left side
    // while loops to get the cell associated with row to check and col to check. This way we check both the left and right side
    while (colToCheck >= 0) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            colToCheck--;// to go more left
        } else {
            break;
        }
    }
    
    colToCheck = colIndex + 1;
    while (colToCheck <= 6) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            colToCheck++;// to go more right
        } else {
            break;
        }
    }

    let isWinningCombo = checkWinningCells(winningCells);
    if (isWinningCombo) return;


    // Check vertically
    winningCells = [cell];
    rowToCheck = rowIndex - 1;
    colToCheck = colIndex; // start from the left side
    // while loops to get the cell associated with row to check and col to check. This way we check both the left and right side
    while (rowToCheck >= 0) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck--;// to go more up
        } else {
            break;
        }
    }
    
    rowToCheck = rowIndex + 1;
    while (rowToCheck <= 5) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck++;// to go more down
        } else {
            break;
        }
    }

    isWinningCombo = checkWinningCells(winningCells);
    if (isWinningCombo) return;



    // Check diagonally 
    //diagonal from bottom left to top right
    winningCells = [cell];
    rowToCheck = rowIndex + 1;
    colToCheck = colIndex - 1; // start from the left side
    // while loops to get the cell associated with row to check and col to check. This way we check both the left and right side
    while (colToCheck >= 0 && rowToCheck <= 5) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck++;
            colToCheck--;
        } else {
            break;
        }
    }
    
    rowToCheck = rowIndex - 1;
    colToCheck = colIndex + 1;
    while (colToCheck <= 6 && rowToCheck >= 0) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck--;
            colToCheck++;
        } else {
            break;
        }
    }

    isWinningCombo = checkWinningCells(winningCells);
    if (isWinningCombo) return;


    // Check diagonally 
    //diagonal from bottom right to top left
    winningCells = [cell];
    rowToCheck = rowIndex - 1;
    colToCheck = colIndex - 1; // start from the right side
    // while loops to get the cell associated with row to check and col to check. This way we check both the right and left side
    while (colToCheck >= 0 && rowToCheck >= 0) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck--;
            colToCheck--;
        } else {
            break;
        }
    }
    
    rowToCheck = rowIndex + 1;
    colToCheck = colIndex + 1;
    while (colToCheck <= 6 && rowToCheck <= 5) {
        const cellToCheck = rows[rowToCheck][colToCheck];
        if (getColorOfCell(cellToCheck) === color) {
            winningCells.push(cellToCheck);
            rowToCheck++;
            colToCheck++;
        } else {
            break;
        }
    }

    isWinningCombo = checkWinningCells(winningCells);
    if (isWinningCombo) return;

    // check to see if there is a tie
    const rowWithoutTop = rows.slice(0, 6);
    for (const row of rowWithoutTop) {
        for (const cell of row) {
            const classList = getClassListArray(cell);
            if (!classList.includes('black') && !classList.includes('red')) {
                return;
            }
        }
    }
    gameIsOn = false;
    statusSpan.textContent = "Game is a Draw!";
}

// Event Handlers
const handleCellMouseHover = function (evt) {
    if (!gameIsOn) return;
    // grabs the cell from the mouse event
    const cell = evt.target;
    // destructured row index and col index using ES6 destructuring notation (property) to get cell location
    const [rowIndex, colIndex] = getCellLocation(cell);
    // show disc color on top of board when mouse hovers the game board. Can also be expressed as:
    // topCell.classList.add(yellowIsNext ? 'yellow' : 'red');
    const topCell = topCells[colIndex];
    if (blackTurn) {
        topCell.classList.add('black');
    } else {
        topCell.classList.add('red');
    }
}

// function to make disc dissapear when mouse is not hovering top of board
// MDN resource: https://developer.mozilla.org/en-US/docs/Web/API/Element/mouseout_event
const handleCellMouseOut = function (evt) {
    const cell = evt.target;
    const [rowIndex, colIndex] = getCellLocation(cell);
    clearColorFromTop(colIndex);
}

// function finds the first available cell location and fills it upon clicking a column 
const handleCellClick = function (evt) {
    if (!gameIsOn) return;
    const cell = evt.target;
    const [rowIndex, colIndex] = getCellLocation(cell);
    
    const openCell = getFirstOpenCellInColumn(colIndex);
// created a conditional that allows the disc to drop from the last cell to the top
    if(!openCell) {
        return;
    } else if(blackTurn) {
        openCell.classList.add('black');
    } else {
        openCell.classList.add('red');
    }
    // call the status of the game function
    checkStatusOfGame(openCell);
// toggles the color of disc to alternate between players upon click
    blackTurn = !blackTurn;
    clearColorFromTop(colIndex);
    if (gameIsOn) {
        const topCell = topCells[colIndex];
        if (blackTurn) {
        topCell.classList.add('black');
        } else {
        topCell.classList.add('red');
        }
    }
    
    
}




// Add Event Listeners using for-of loops
for (const row of rows) {
    for (const cell of row) {
        cell.addEventListener('mouseover', handleCellMouseHover);
        cell.addEventListener('mouseout', handleCellMouseOut);
        cell.addEventListener('click', handleCellClick);
    }
}

// create reset function event listener to reset the game using for-of loops
resetButton.addEventListener('click', function() {
    for (const row of rows) {
        for (const cell of row) {
            cell.classList.remove('red');
            cell.classList.remove('black');
            cell.classList.remove('win');
        }
    }
    gameIsOn = true;
    blackTurn = true;
    statusSpan.textContent = '';
})